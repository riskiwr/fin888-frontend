@extends('template.master')
@section('judul')
Member Area
@endsection


@push("datepicker")
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@endpush
@section('content')

<section class="bg-white m-3 shadow-sm py-3 px-2" id="top-up">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">

                @include('member-area.nav')
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-auto">
                <label for="" class="font-weight-bold">Period</label>
                <div class="input-group mb-3" style="width:133px;">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-white border-right-0 border-main text-main">Day :</span>
                    </div>
                    <select class="form-control border-left-0 border-main text-main" id="">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            <div class="col-auto">
                <label for="" class="font-weight-bold">Range</label>
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text bg-white border-main rounded-left">
                            <i class="far fa-calendar text-main"></i>
                        </span>
                    </div>
                    <input data-toggle="datepicker" type="text" name="name" class="form-control border-left-0 border-main" placeholder="Select a date" autocomplete="off">
                </div>
            </div>
        </div>


        <div class="table-responsive-md">

            <table class="table">
                <thead>
                    <tr>
                        <th class="border-top-0">Account</th>
                        <th class="border-top-0">Account Type</th>
                        <th class="border-top-0">Login ID</th>
                        <th class="border-top-0">Leverage</th>
                        <th class="border-top-0">Master Password</th>
                        <th class="border-top-0">Investor Password</th>
                        <th class="border-top-0">Balance</th>
                        <th class="border-top-0">Credit</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Trade copiying account</td>
                        <td>FIN888</td>
                        <td>1231231231</td>
                        <td>1:100</td>
                        <td>qwerQTh123</td>
                        <td>werwuer!@#</td>
                        <td>USD 1.0</td>
                        <td>USD 2.0</td>
                    </tr>

                </tbody>
            </table>
        </div>

        <div class="row justify-content-between">
            <div class="col-auto">
                Showing 1 - 10 of 88 entries
            </div>
            <div class="col-auto">
                <nav>
                    <ul class="pagination pagination-main">
                        <li class="page-item">
                            <a class="page-link text-main" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item text"><a class="page-link" href="#">1</a></li>
                        <li class="page-item active"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

</section>



<script>
    $('[data-toggle="datepicker"]').datepicker();
</script>
@endsection