<nav class="nav nav-page align-items-center">
  <a class="nav-link @if(Request::segment(2) == 'commision-payout') {{ 'active'}} @endif" href="/member-area/commision-payout">Commision Payout</a>
  <a class="nav-link  @if(Request::segment(2) == 'affiliate-network-payouts') {{ 'active'}} @endif" href="/member-area/affiliate-network-payouts">Affiliate Network Payouts</a>
  <a class="nav-link  @if(Request::segment(2) == 'trading-rebate-payouts') {{ 'active'}} @endif" href="/member-area/trading-rebate-payouts">Trading Rebate Payouts</a>

</nav>