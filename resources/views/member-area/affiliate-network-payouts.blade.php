@extends('template.master')
@section('judul')
Member Area
@endsection


@section('content')
<section class="bg-white m-3 shadow-sm py-3 px-2" id="top-up">
    @include('member-area.nav')
    <div class="container-fluid">
        <div class="row justify-content-between mb-5">
            <div class="col-lg-5">
                <label for="">Referal Link</label>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="https://www.thefin888.com/jhon.doe99" value="https://www.thefin888.com/jhon.doe99" aria-label="Recipient's username" aria-describedby="button-addon2" disabled>
                    <div class="input-group-append">
                        <button class="btn border" type="button" id="button-addon2"><img src="{{ asset('img/icon/copy.svg') }}" alt=""></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <label for="" class="mb-4">Total Balance</label>
                <h6>USD 2,000</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="border-top-0">#</th>
                            <th class="border-top-0">Investor Name</th>
                            <th class="border-top-0">Nominal</th>
                            <th class="border-top-0">under Tree</th>
                            <th class="border-top-0"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>Jhon Doe</td>
                            <td>USD 10,500</td>
                            <td>5</td>
                            <td>>></td>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>Jhon Doe</td>
                            <td>USD 10,500</td>
                            <td>5</td>
                            <td>>></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-5">
                <div class="bg-white shadow py-5 px-3">
                    <h5>Jhon Doe</h5>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Nominal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Megan Fox</td>
                                <td>USD100</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection