@extends('template.master')
@section('judul')
Dashboard
@endsection

@section('content')
<section class="stats py-5">
    <div class="container-fluid">
        <div class="row mb-3 justify-content-between">
            <div class="col-auto">
                <h4 class="font-weight-bolder">Welcome, Jimmy Raikkonen</h4>
            </div>
            <div class="col-auto">
                <div class=" d-table">
                    <a href="#" class="btn btn-main mx-2"><img src="{{ asset('img/icon/withdraw.svg') }}" alt="" class="mr-1"> Withdraw</a>
                    <a href="#" class="btn btn-main mx-2"><img src="{{ asset('img/icon/topup.svg') }}" alt="" class="mr-1">Top Up</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-3">
                    <div class="bg-white rounded shadow">

                        <div class="p-3">
                            <div class="d-flex">
                                <h5 class="font-weight-bold">Live</h5>
                                <h5 class="font-weight-bold ml-auto"><i class="fas fa-ellipsis-v"></i></h5>
                            </div>
                            <div style="position: relative;height:200px">
                                <canvas id="liveWalletChart"></canvas>
                                <script>
                                    new Chart(document.getElementById('liveWalletChart'), {
                                        type: 'bar',
                                        data: {
                                            labels: ['', '', '', '', '', '', '', '', '', '', '', ''],
                                            datasets: [{
                                                label: '',
                                                data: [100, 200, 300, 200, 400, 100, 200, 300, 100, 300, 400, 200, 100],
                                                backgroundColor: '#FBBC06',

                                            }]
                                        },
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                            }
                                        }
                                    });
                                </script>
                            </div>
                        </div>


                        <div class="px-3 py-2 border-top">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <p class="mr-3 mb-0">Total :</p>
                                    <h6 class="font-weight-bold mb-0">USD 400</h6>
                                </div>
                                <div class="d-flex text-success align-items-center">
                                    <i class="fas fa-arrow-up mr-3"></i>
                                    <p class="mb-0">23.04%</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 mb-3">
                    <div class="bg-white rounded shadow">
                        <div class="p-3">
                            <div class="d-flex">
                                <h5 class="font-weight-bold">Transit</h5>
                                <h5 class="font-weight-bold ml-auto"><i class="fas fa-ellipsis-v"></i></h5>
                            </div>
                            <div style="position: relative;max-width:100%;height:200px">
                                <canvas id="transitWalletChart"></canvas>
                                <script>
                                    new Chart(document.getElementById('transitWalletChart'), {
                                        type: 'bar',
                                        data: {
                                            labels: ['', '', '', '', '', '', '', '', '', '', '', ''],
                                            datasets: [{
                                                label: '',
                                                data: [100, 200, 300, 200, 400, 100, 200, 300, 100, 300, 400, 200, 100],
                                                backgroundColor: '#FBBC06',

                                            }]
                                        },
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                            }
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="px-3 py-2 border-top">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <p class="mr-3 mb-0">Saldo:</p>
                                    <h6 class="font-weight-bold mb-0">USD 400</h6>
                                </div>
                                <div class="d-flex text-success align-items-center">
                                    <i class="fas fa-arrow-up mr-3"></i>
                                    <p class="mb-0">23.04%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="my-5">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="bg-white rounded p-5 shadow" style="position:relative;height:100vh">
                    <div>
                        <h5 class="font-weight-bold">Trade Status</h5>

                    </div>
                    <div style="position: relative;width:100%;height:400px">
                        <canvas id="tradeStatusChart"></canvas>
                        <script>
                            new Chart(document.getElementById('tradeStatusChart'), {
                                type: 'bar',
                                data: {
                                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                                    datasets: [{
                                        label: '',
                                        data: [100, 200, 300, 200, 400, 100, 200, 300, 100, 300, 400, 200, 100],
                                        backgroundColor: '#007BFF',
                                    }]
                                },
                                options: {
                                    responsive: true,
                                    maintainAspectRatio: false,
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="my-5">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="bg-white rounded p-5 shadow ">
                    <h5 class="font-weight-bold">My Affiliate</h5>
                    <div class="table-responsive-md">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th class="border-top-0">#</th>
                                    <th class="border-top-0">Name</th>
                                    <th class="border-top-0">Email</th>
                                    <th class="border-top-0">Phone Number</th>
                                    <th class="border-top-0">Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x</td>
                                    <td>x</td>
                                    <td>x</td>
                                    <td>x</td>
                                    <td><span class="badge badge-warning px-5 py-2 text-white">Executive</span></td>
                                </tr>
                                <tr>
                                    <td>x</td>
                                    <td>x</td>
                                    <td>x</td>
                                    <td>x</td>
                                    <td><span class="badge badge-secondary px-5 py-2 text-white">Member</span></td>
                                </tr>
                                <tr>
                                    <td>x</td>
                                    <td>x</td>
                                    <td>x</td>
                                    <td>x</td>
                                    <td><span class="badge badge-wood px-5 py-2 text-white">Manager</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection