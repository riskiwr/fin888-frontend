@extends("auths.template.master")

@section('content')

<section id="register-1">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">

                <div class="text-center mb-5">
                    <h1 class="font-weight-bold">{{ __('home.re_judul') }}</h1>
                    <p>{{ __('home.re_subjudul') }}</p>
                </div>
            
                <div class="stepbar mb-5">
                    <div class="row justify-content-between">
                        <div class="col-auto active">
                            <div class="text-center">
                                <h6>{{ __('home.re_acc') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="text-center">
                                <h6>{{ __('home.re_ident') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="text-center">
                                <h6>{{ __('home.re_doc') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <form method="POST" action="{{ URL::to('register-1') }}">
                    @csrf
                    <div class="form-group">
                        <label for="" class="font-weight-bolder">{{ __('home.re_em') }}</label>
                        <input type="email" name="email" class="form-control" placeholder="{{ __('home.re_Pem') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-weight-bolder">{{ __('home.re_create') }}</label>
                        <div class="input-group">
                            <input type="text" name="username" class="form-control" placeholder="{{ __('home.re_Pcreate') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text  border-0 bg-white ">
                                    <div class="username-check rounded-circle p-1">
                                        <i class=" fas fa-check"></i>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-weight-bolder">{{ __('home.re_upline') }}</label>
                        <input type="text" name="upline_username" class="form-control" placeholder="{{ __('home.re_Pupline') }}" required>
                    </div>
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.re_pass') }}</strong></label>
                        <div class="input-group">
                            <input type="password" name="password"  class="form-control border-right-0" placeholder="{{ __('home.re_Ppass') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-0 bg-white "><i class="toggle-password far fa-eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.re_cpass') }}</strong></label>
                        <div class="input-group">
                            <input type="password" name="retype_password" class="form-control border-right-0" placeholder="{{ __('home.re_Pcpass') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-0 bg-white "><i class="toggle-password far fa-eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-main w-100"  type="submit"> {{ __('home.re_next') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    document.querySelectorAll('.toggle-password').forEach(function(e) {
        e.addEventListener("click", function(el) {
            const input = el.target.parentElement.parentElement.previousElementSibling
            console.log(input);
            if (input.getAttribute("type") == "password") {
                input.setAttribute("type", "text")
                el.target.classList.remove("fa-eye")
                el.target.classList.add("fa-eye-slash")
            } else {
                input.setAttribute("type", "password")
                el.target.classList.remove("fa-eye-slash")
                el.target.classList.add("fa-eye")
            }
        })
    })
</script>
@endsection