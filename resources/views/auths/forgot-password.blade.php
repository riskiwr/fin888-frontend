@extends("auths.template.master")

@section('content')
<section id="forgot-password">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="text-center mb-5">
                    <h1 class="font-weight-bold mb-3">Forgot Password</h1>
                    <p>lorem ipsum dolor sit amet</p>
                </div>
                <form action="javascript:alert(grecaptcha.getResponse(widgetId1));" method="POST">
                    <div class="form-group mb-3">
                        <label for="" class="font-weight-bold">Input username</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group mb-5">
                        <div class="g-recaptcha d-table mx-auto" data-sitekey="6Lc5sfQZAAAAABa0x1WpBO3jWTvzRJHCJajZZHjb"></div>
                    </div>
                    <button class="btn btn-main w-100" type="submit" value="Submit">Submit</button>
                </form>

            </div>
        </div>
    </div>
</section>

<!-- <script type="text/javascript">
    var onloadCallback = function() {
        alert("grecaptcha is ready!");
    };
    </script> -->

@endsection