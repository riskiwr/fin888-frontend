@extends("auths.template.master")

@push("datepicker")
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@endpush
<?php

use Illuminate\Support\Facades\Http;

$response = Http::get('https://restcountries.eu/rest/v2/all');
$country = $response->json();
//  dd($country);
?>
@section('content')


<section id="register-2">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="text-center mb-5">
                    <h1 class="font-weight-bold">{{ __('home.r2_judul') }}</h1>
                    <p>{{ __('home.r2_subjudul') }}</p>
                </div>

                <div class="stepbar mb-5">
                    <div class="row justify-content-between">
                        <div class="col-auto ">
                            <div class="text-center">
                                <h6>{{ __('home.r2_acc') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto active">
                            <div class="text-center">
                                <h6>{{ __('home.r2_ident') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="text-center">
                                <h6>{{ __('home.r2_doc') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                    </div>
                </div>


                <form action="">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_first') }}</label>
                                    <input type="text" class="form-control" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_bdp') }}</label>
                                    <input type="text" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_bdd') }}</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text  border-right-0 bg-white ">
                                                <i class="far fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input data-toggle="datepicker" type="text" name="name" class="form-control border-left-0" placeholder="{{ __('home.r2_Pbdd') }}" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_gender') }}</label>
                                    <div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="man" name="gender" class="custom-control-input">
                                            <label class="custom-control-label" for="man">{{ __('home.r2_men') }}</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="women" name="gender" class="custom-control-input">
                                            <label class="custom-control-label" for="women">{{ __('home.r2_women') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_phone') }}</label>
                                    <input type="number" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_idtype') }}</label>
                                    <select class="form-control" id="">
                                        <option selected="">ID CARD</option>
                                        <option>Driving License </option>
                                        <option>Passport </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_idcard') }}</label>
                                    <input type="number" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_country') }}</label>
                                    <select class="form-control" id="">
                                        <option selected>Chosee..</option>
                                        @foreach($country as $cnt)
                                            <option>{{ $cnt['name'] }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_postal') }}</label>
                                    <input type="number" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="" class="font-weight-bold">{{ __('home.r2_add') }}</label>
                                    <textarea name="" id="" rows="5" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-main w-100" disabled>{{ __('home.r2_next') }}</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $('[data-toggle="datepicker"]').datepicker();
</script>

@endsection