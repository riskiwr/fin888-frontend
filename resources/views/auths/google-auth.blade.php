@extends("auths.template.master")

@section('content')
    <section id="google-auth">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="text-center mb-5">
                        <h1 class="font-weight-bold mb-3">Please verify your account</h1>
                        <p>Please enter your OTP Google Auth, that we send <br> to <b> john.doe@gmail.com</b></p>
                    </div>

                    <div class="text-center mb-3">
                        <h6 class="font-weight-bold">Code</h6>
                    </div>

                    <form action="">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" maxlength="1" placeholder="__">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" maxlength="1" placeholder="__">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" maxlength="1" placeholder="__">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" maxlength="1" placeholder="__">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" maxlength="1" placeholder="__">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" maxlength="1" placeholder="__">
                            </div>
                        </div>
                        <div class="row justify-content-center my-5">
                            <div class="col-lg-8">
                                <button class="btn btn-main w-100">Verification</button>
                            </div>
                        </div>
                        <div class="text-center">
                            <p>Didn’t receive? <a href="#">Resent</a></p>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </section>
@endsection