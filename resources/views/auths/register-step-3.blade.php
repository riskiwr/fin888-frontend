@extends("auths.template.master")
@push("bs-input")
<script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
@endpush
@section('content')
<section id="register-3">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="text-center mb-5">
                    <h1 class="font-weight-bold">{{ __('home.r3_judul') }}</h1>
                    <p>{{ __('home.r3_subjudul') }}</p>
                </div>

                <div class="stepbar mb-5 ">
                    <div class="row justify-content-between ">
                        <div class="col-auto ">
                            <div class="text-center">
                                <h6>{{ __('home.r3_acc') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="text-center">
                                <h6>{{ __('home.r3_ident') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                        <div class="col-auto active">
                            <div class="text-center">
                                <h6>{{ __('home.r3_doc') }}</h6>
                                <i class="fas fa-circle"></i>
                            </div>
                        </div>
                    </div>
                </div>



                <form action="">
                    <div class="row align-items-center mb-5">
                        <div class="col-auto">
                            <div class="rounded-circle" style="height: 100px;width:100px;overflow:hidden">
                                <img src="{{ asset('img/selfie.svg') }}" alt="" height="100" id="previewImg" accept="image/x-png,image/jpeg" onclick="test();" class="img-fluid">
                            </div>
                            <input class="d-none" type="file" name="photo" onchange="previewFile(this);" required id="imgFile" accept=".png, .jpg, .jpeg" >
                        </div>
                        <div class="col-auto">
                            <h6 class="font-weight-bold">{{ __('home.r3_upself') }}</h6>
                            <h6 class="font-italic">{{ __('home.r3_max25') }}</h6>
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <label for="" class="text-secondary"><small class="font-weight-bold">{{ __('home.r3_front') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" accept=".png, .jpg, .jpeg" required>
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.r3_upload') }}">{{ __('home.r3_choose') }}</label>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="form-group mb-4">
                        <label for="" class="text-secondary"><small class="font-weight-bold">{{ __('home.r3_back') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" accept=".png, .jpg, .jpeg" required>
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.r3_upload') }}">{{ __('home.r3_choose') }}</label>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="form-group mb-4">
                        <label for="" class="text-secondary"><small class="font-weight-bold">{{ __('home.r3_proof') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" accept=".png, .jpg, .jpeg" required>
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.r3_upload') }}">{{ __('home.r3_choose') }}</label>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="check" required>
                        <label class="form-check-label text-secondary" for="check">
                            <small class="font-italic">{{ __('home.r3_aggree') }}</small>
                        </label>
                    </div>
                    <div class="form-group my-5">
                        <button class="btn btn-main w-100">{{ __('home.r3_finish') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        bsCustomFileInput.init()
    })
</script>
<script>
    function previewFile(input) {
        var file = $("input[type=file]").get(0).files[0];

        if (file) {
            var reader = new FileReader();

            reader.onload = function() {
                $("#previewImg").attr("src", reader.result);
            }

            reader.readAsDataURL(file);
        }
    }
</script>
<script>
    function test() {
        $("input[id='imgFile']").click();
    }
</script>

@endsection