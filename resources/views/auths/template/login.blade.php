@extends("auths.template.master")

@section('content')
<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="title text-center">
                    <h1 class="font-weight-bold mb-3">{{ __('home.log_judul') }}</h1>
                    <p>{{ __('home.log_subjudul') }} </p>
                </div>
                <form method="POST" action="{{ URL::to('login') }}">
                    <div class="form-group mb-3">
                        <label for="" class="font-weight-bold">{{ __('home.log_em') }}</label>
                        <input id="user" type="text" name="user" class="form-control" placeholder="{{ __('home.log_Pem') }}" required autofocus>
                    </div>
                    <div class="form-group mb-3">
                        <label for="" class="font-weight-bold">{{ __('home.log_pass') }}</label>
                        <div class="input-group mb-3">
                            <input id="password" type="password" name="password" class="form-control border-right-0" placeholder="{{ __('home.log_Ppass') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-0 bg-white "><i class="toggle-password far fa-eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group my-3">
                        <div class="g-recaptcha d-table m-auto" data-sitekey="6Lc5sfQZAAAAABa0x1WpBO3jWTvzRJHCJajZZHjb" data-size="normal"></div>
                    </div>
                    {{ csrf_field() }}
                    <div class="form-group mb-3">
                        <a href="#"><small>{{ __('home.log_forgot') }}</small></a>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-main w-100">{{ __('home.log_submit') }}</button>
                    </div>
                    <div class="form-group text-center">
                        <p>{{ __('home.log_or') }}</p>
                        <p>{{ __('home.log_donot') }} <a href="/register-1">{{ __('home.log_create') }}</a> </p>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>
<script>
    document.querySelectorAll('.toggle-password').forEach(function(e) {
        e.addEventListener("click", function(el) {
            const input = el.target.parentElement.parentElement.previousElementSibling
            console.log(input);
            if (input.getAttribute("type") == "password") {
                input.setAttribute("type", "text")
                el.target.classList.remove("fa-eye")
                el.target.classList.add("fa-eye-slash")
            } else {
                input.setAttribute("type", "password")
                el.target.classList.remove("fa-eye-slash")
                el.target.classList.add("fa-eye")
            }
        })
    })
</script>



@endsection