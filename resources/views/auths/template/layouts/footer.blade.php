{{-- FOOTER --}}
<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row justify-content-between align-items-end">
                    <div class="col-6"> 
                        <img src="{{ asset('img/fin888-logo.png')}}" alt="Logo FIN888" height="50">
                    </div>
                    <div class="col-6 text-right">
                        <h5>About Us</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr class="border">
        <div class="row">
            <div class="col-12">
                <div class="row justify-content-between">
                    <div class="col-6">
                        <p>Copyright © 2020 fin888</p>
                    </div>
                    <div class="col-6 text-right">
                        <div class="d-flex">
                            Social Media :
                            <div class="d-flex">
                                <img src="{{ asset('img/icon/twitter.svg') }}" alt="">
                                <img src="{{ asset('img/icon/twitter.svg') }}" alt="">
                                <img src="{{ asset('img/icon/twitter.svg') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>