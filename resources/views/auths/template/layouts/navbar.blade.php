{{-- NAVBAR --}}
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light align-items-center">
            <a class="navbar-brand" href="#"><img src="{{ asset('img/fin888-logo.png')}}" alt="logo fin888" height="40"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <a class="nav-link active mx-2" href="/login">Login <span class="sr-only">(current)</span></a>
                    <a class="nav-link btn btn-main mx-2" href="/register-1">Register</a>
                    <div class="dropdown ml-5">
                        <a class="dropdown-toggle btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true">
                            <img src="{{ asset('img/flag-en.svg') }}" height="20">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item {{ app()->getLocale() == 'en' ? 'active' : '' }}"  href="{{ route('localization.switch', 'en') }}"><B>English</B> </a>
                        <a class="dropdown-item {{ app()->getLocale() == 'id' ? 'active' : '' }}" href="{{ route('localization.switch', 'id') }}"><B>Indonesia</B> </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
{{-- END NAVBAR --}}