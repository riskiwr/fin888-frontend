<nav id="topbar">
    <div class="d-flex justify-content-between px-3 py-3 align-items-center">
        <div class="mr-3">
            <button type="button" id="sidebarCollapse" class="btn">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle Menu</span>
            </button>
        </div>
        <div class="page-title h5 font-weight-bold mb-0">
            @yield('judul')
        </div>

        <div class="ml-auto">
            <div class="d-flex">
                <div class="dropdown ml-5">
                    <a class="dropdown-toggle btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true">
                        <img src="{{ asset('img/flag-en.svg') }}" height="20">
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item {{ app()->getLocale() == 'en' ? 'active' : '' }}" href="{{ route('localization.switch', 'en') }}"><B>English</B> </a>
                        <a class="dropdown-item {{ app()->getLocale() == 'id' ? 'active' : '' }}" href="{{ route('localization.switch', 'id') }}"><B>Indonesia</B> </a>

                    </div>
                </div>
                <div class="border border-left mx-3"></div>
                <div class="dropdown">
                    <a class="dropdown-toggle btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true">
                        <img src="{{ asset('img/profile.png') }}" height="20">
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ URL::to('logout') }}">Logout</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>