<aside id="sidebar" class="active">

    <ul class="list-unstyled components mt-5">
        <li class="@if(Request::segment(1) == 'dashboard') {{ 'active' }} @endif">
            <a href="/dashboard">
                <span> <i><img src="{{ asset('img/icon/dashboard.svg') }}" alt=""></i></span> Dashboard
            </a>
        </li>
        <li class="@if(Request::segment(1) == 'wallet') {{ 'active' }} @endif">
            <a href="/wallet"><span><i><img src="{{ asset('img/icon/wallet.svg') }}" alt=""></i> </span> Wallet</a>
        </li>
        <li class=" @if(Request::segment(1) == 'account-details') {{ 'active' }} @endif">
            <a href="/account-details"><span><i><img src="{{ asset('img/icon/account-details.svg') }}" alt=""></i></span> Account Details</a>
        </li>
        <li class=" @if(Request::segment(1) == 'trading-account') {{ 'active' }} @endif">
            <a href="/trading-account"><span><i><img src="{{ asset('img/icon/trading-account.svg') }}" alt=""></i></span> Trading Account</a>
        </li>
        <li class=" @if(Request::segment(1) == 'member-area') {{ 'active' }} @endif">
            <a href="/member-area"><span><i><img src="{{ asset('img/icon/member-area.svg') }}" alt=""></i></span>Member Area</a>
        </li>
    </ul>

</aside>
