@extends('template.master')
@section('judul')
     Account Detail
@endsection

@section('content')
<section class="bg-white mt-4 mx-3 px-3 py-3 h-100">
    @include('account-details.nav')

    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="text-center">
                    <h3 class="font-weight-bolder">{{ __('home.cp_change') }}</h3>
                </div>
                <form action="" class="mt-5">
                    <div class="form-group mb-4">
                        <label for="" class="font-weight-bold">{{ __('home.cp_old') }}</label>
                        <div class="input-group">
                            <input type="password" class="form-control border-right-0" placeholder="{{ __('home.cp_old2') }}">
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-0 bg-white "><i class="toggle-password far fa-eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <label for="" class="font-weight-bold">{{ __('home.cp_newpass') }}</label>
                         <div class="input-group">
                            <input type="password" class="form-control border-right-0" placeholder="{{ __('home.cp_newpass2') }}">
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-0 bg-white "><i class="toggle-password far fa-eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <label for="" class="font-weight-bold">{{ __('home.cp_verif_pass') }}</label>
                         <div class="input-group">
                            <input type="password" class="form-control border-right-0" placeholder="{{ __('home.cp_verif_pass2') }}">
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-0 bg-white "><i class="toggle-password far fa-eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mx-auto d-table">
                            <a href="" class="btn btn-outline-danger mx-2 px-5">{{ __('home.cp_cancel') }}</a>
                            <a href="" class="btn btn-main mx-2 px-5">{{ __('home.cp_save') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    document.querySelectorAll('.toggle-password').forEach(function(e) {
        e.addEventListener("click", function(el) {
            const input = el.target.parentElement.parentElement.previousElementSibling
            console.log(input);
            if (input.getAttribute("type") == "password") {
                input.setAttribute("type", "text")
                el.target.classList.remove("fa-eye")
                el.target.classList.add("fa-eye-slash")
            } else {
                input.setAttribute("type", "password")
                el.target.classList.remove("fa-eye-slash")
                el.target.classList.add("fa-eye")
            }
        })
    })
</script>
@endsection