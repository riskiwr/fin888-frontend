@extends('template.master')
@section('judul')
     Account Detail
  @endsection

@push("bs-file")
<script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
@endpush

@section('content')
<section class="bg-white mt-4 mx-3 px-3 py-3 h-100">

    @include('account-details.nav')


    <div class="container-fluid mt-5">
        <form action="">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.nom_first') }}</strong></label>
                        <input type="text" name="Firstname" class="form-control" placeholder="{{ __('home.nom_Pfirst') }}">
                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.nom_phone') }}</strong></label>
                        <input type="text" name="Phonenumber" class="form-control" placeholder="{{ __('home.nom_Pphone') }}">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.nom_email') }}</strong></label>
                        <input type="email" name="Email" class="form-control" placeholder="{{ __('home.nom_Pemail') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="validationDefault04"><strong>{{ __('home.nom_idtype') }}</strong></label>
                        <select class="form-control" id="validationDefault04" required>
                            <option selected disabled value="">Id Card Type</option>
                            <option>Id Card </option>
                            <option>Driving License </option>
                            <option>Passport </option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.nom_idnumber') }}</strong></label>
                        <input type="number" name="Id card number" class="form-control" placeholder="{{ __('home.nom_Pidnumber') }}">
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="validationDefault04"><small>{{ __('home.nom_front') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.nom_upload') }}">{{ __('home.nom_choose') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="validationDefault04"><small>{{ __('home.nom_back') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.nom_upload') }}">{{ __('home.nom_choose') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="validationDefault04"><small>{{ __('home.nom_proof') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.nom_upload') }}">{{ __('home.nom_choose') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="validationDefault04"><small>{{ __('home.nom_wills') }}</small></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.nom_upload') }}">{{ __('home.nom_choose') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for=""><strong>{{ __('home.nom_add') }}</strong></label>
                        <textarea name="" id="" cols="30" rows="4" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-auto">
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-danger mr-2 px-5"> {{ __('home.cp_cancel') }} </button>
                        <button type="submit" class="btn btn-main px-5"> {{ __('home.cp_save') }} </button>
                    </div>
                </div>
            </div>
        </form>
    </div>










    <div class="form-group row justify ml-3">

    </div>
    </div>

</section>


<script>
    $(document).ready(function() {
        bsCustomFileInput.init()
    })
</script>

<script>
    document.querySelectorAll('.toggle-password').forEach(function(e) {
        e.addEventListener("click", function(el) {
            const input = el.target.parentElement.parentElement.previousElementSibling
            console.log(input);
            if (input.getAttribute("type") == "password") {
                input.setAttribute("type", "text")
                el.target.classList.remove("fa-eye")
                el.target.classList.add("fa-eye-slash")
            } else {
                input.setAttribute("type", "password")
                el.target.classList.remove("fa-eye-slash")
                el.target.classList.add("fa-eye")
            }
        })
    })
</script>





@endsection