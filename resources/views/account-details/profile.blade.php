@extends('template.master')
@section('judul')
Account Detail
@endsection

@section('content')
<section class="bg-white mt-4 mx-3 px-3 py-3 text-secondary">
    @include('account-details.nav')

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-lg-4 border-right mb-3">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <img src="{{ asset('img/elipse6.svg') }}" alt="" height="100">
                    </div>
                    <div class="col-auto">
                        <h5 class="font-weight-bold">Jimmy Raikkonen</h5>
                        <p class="text-black-50">@jimmyraikonen</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 border-right mb-3">
                <div class="row">
                    <div class="col-12">
                        <p>Jimmyraikkoo09@company.com</p>
                    </div>
                    <div class="col-12">
                        <p>+6281345831264</p>
                    </div>
                    <div class="col-12">
                        <p>Executive Manager</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-3">
                <div class="row">
                    <div class="col-12">
                        <p>Member since: 23/04/2016</p>
                    </div>
                    <div class="col-12">
                        <p>Jl. Cilandak Tengah 3 No.48, RT.1/RW.1, Cilandak Bar., Kec. Cilandak, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12430</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-auto">
                <h5 class="font-weight-bolder">{{ __('home.prof_info') }}</h5>
                <table class="table table-borderless font-weight-bold ">
                    <tr>
                        <td>{{ __('home.prof_nation') }}</td>
                        <td>: Indonesia</td>
                    </tr>
                    <tr>
                        <td>{{ __('home.prof_idnumber') }}</td>
                        <td>: 12313123123123</td>
                    </tr>
                    <tr>
                        <td>{{ __('home.prof_gender') }}</td>
                        <td>: Man</td>
                    </tr>
                    <tr>
                        <td>{{ __('home.prof_pob') }}</td>
                        <td>: Jakarta</td>
                    </tr>

                </table>
            </div>
            <div class="col-auto">
                <h5 class="font-weight-bold">2 Factor Auth</h5>
                <label class="switch">
                    <input type="checkbox">
                    <span class="slider round"></span>
                </label>
                <div>
                    <img src="{{  asset('img/ex-code.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>





@endsection