@extends('template.master')
@section('judul')
     Account Detail
  @endsection
@push("bs-file")
<script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
@endpush
@section('content')
<section class="bg-white mt-4 mx-3 px-3 py-3  h-100" id="documents">

	@include('account-details.nav')
	<div class="container-fluid mt-5">
		<div class="mb-5">
			<button type="button" class="btn btn-primary ml-auto d-table" type="submit" style="background-color: #2d3da8" data-toggle="modal" data-target="#exampleModal">
				{{ __('home.dok_upload') }}
			</button>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th class="border-top-0">{{ __('home.dok_type') }}</th>
					<th class="border-top-0">{{ __('home.dok_com') }}</th>
					<th class="border-top-0">{{ __('home.dok_stat') }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td> Front side of Identity Card </td>
					<td> - </td>
					<td><span class="badge badge-success px-4 py-2 w-75">Approved</span></td>
				</tr>
				<tr>
					<td> Back side of Identity Card </td>
					<td> Photo blur, please reupload with better camera </td>
					<td> <span class="badge badge-danger px-4 py-2 w-75">Failed</span></td>
				</tr>
				<tr>
					<td> Proof of resedential Address </td>
					<td> - </td>
					<td><span class="badge badge-danger px-4 py-2 w-75">Failed</span></td>
				</tr>
				<tr>
					<td> Selfie With Identity Card </td>
					<td> - </td>
					<td> <span class="badge badge-success px-4 py-2 w-75">Success</span></td>
				</tr>
			</tbody>
		</table>
	</div>


	<!-- Modal -->
	<div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content p-5">
				<div class="modal-header border-0 justify-content-center">
					<h5 class="modal-title font-weight-bolder text-black " id="exampleModalLabel">{{ __('home.dok_doc') }}</h5>
				</div>
				<div class="modal-body">

					<div class="form-group mb-5">
						<label for="exampleFormControlSelect1" class="font-weight-bold"> {{ __('home.dok_idtype') }} </label>
						<select class="form-control font-weight-bold" id="exampleFormControlSelect1" >
							<option> Document Type </option>
							<option> Document Type </option>
							<option> Document Type </option>
							<option> Document Type </option>
							<option> Document Type </option>
						</select>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">{{ __('home.dok_select') }} </label>
						<div class="input-group mb-3">
							<div class="input-group-prepend ">
								<span class="input-group-text bg-white border-0"><img src="{{ asset('img/book.svg') }}" alt=""></span>
							</div>
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
								<label class="custom-file-label border-0" for="inputGroupFile01" data-browse="{{ __('home.nom_upload') }}">{{ __('home.dok_pdfmodal') }}</label>
							</div>
						</div>
						<hr>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn text-black-50" data-dismiss="modal">
						{{ __('home.dok_close') }}
					</button>
					<button type="button" class="btn btn-main"> {{ __('home.dok_procced') }}
					</button>
				</div>
			</div>
		</div>
	</div>



</section>

<script>
	$(document).ready(function() {
		bsCustomFileInput.init()
	})
</script>
@endsection