<nav class="nav nav-page align-items-center">
  <a class="nav-link @if(Request::segment(2) == 'profile' || Request::segment(2) == '') {{ 'active'}} @endif" href="/account-details/profile">Profile</a>
  <a class="nav-link  @if(Request::segment(2) == 'nomination') {{ 'active'}} @endif" href="/account-details/nomination">Nomination</a>
  <a class="nav-link  @if(Request::segment(2) == 'documents') {{ 'active'}} @endif" href="/account-details/documents">Documents</a>
  <a class="nav-link  @if(Request::segment(2) == 'change-password') {{ 'active'}} @endif" href="/account-details/change-password">Change Password</a>
</nav>