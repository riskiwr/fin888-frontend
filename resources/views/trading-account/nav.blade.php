<nav class="nav nav-page align-items-center">
  <a class="nav-link  @if(Request::segment(2) == 'trading-account') {{ 'active'}} @endif" href="/trading-account/trading-account">Trading Account</a>
  <a class="nav-link  @if(Request::segment(2) == 'trading-history') {{ 'active'}} @endif" href="/trading-account/trading-history">Trading History</a>
  <a class="nav-link  @if(Request::segment(2) == 'trading-performance-report') {{ 'active'}} @endif" href="/trading-account/trading-performance-report">Trading Performance Report</a>

</nav>