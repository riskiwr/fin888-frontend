@extends('template.master')
@section('judul')
    Trading Account
  @endsection
@push("datepicker")
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@endpush
@section('content')
<section class="stats py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="bg-white rounded shadow">
                    <div class="p-3">
                        transit
                        Chart Here
                    </div>
                    <div class="px-3 py-2 border-top">
                        total?s
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bg-white rounded shadow">
                    <div class="p-3">
                        Live
                        Chart Here
                    </div>
                    <div class="px-3 py-2 border-top">
                        total?
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-white m-3 shadow-sm py-3" id="top-up">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">
                @include('trading-account.nav')
            </div>
          
        </div>
      
        <table class="table">
            <thead>
                <tr>
                    <th class="border-top-0">Account</th>
                    <th class="border-top-0">Account Type</th>
                    <th class="border-top-0">Login ID</th>
                    <th class="border-top-0">Leverage</th>
                    <th class="border-top-0">Master Password</th>
                    <th class="border-top-0">Investor Password</th>
                    <th class="border-top-0">Balance</th>
                    <th class="border-top-0">Credit</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Trade copiying account</td>
                    <td>FIN888</td>
                    <td>1231231231</td>
                    <td>1:100</td>
                    <td>qwerQTh123</td>
                    <td>werwuer!@#</td>
                    <td>USD 1.0</td>
                    <td>USD 2.0</td>
                </tr>
              
            </tbody>
        </table>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content p-3">
            <div class="modal-header border-0">
                <div>
                    <h5 class="modal-title font-weight-bolder">Top Up</h5>
                    <p class="font-weight-bold">Top up can be proceed only on monday to thursday</p>
                </div>
            </div>
            <div class="modal-body mt-5">
                <form action="">
                    <div class="form-group">
                        <label for="">Nominal</label>
                        <input type="text" class="form-control">
                    </div>
                    <p>1 USD = xxx USDT</p>
                </form>
            </div>
            <div class="modal-footer border-0 mt-3">
                <button type="button" class="btn text-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-main">Proceed</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('[data-toggle="datepicker"]').datepicker();
</script>
@endsection