@extends('template.master')
@section('judul')
Wallet
@endsection
@push("datepicker")
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@endpush
@section('content')
<section class="stats py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="bg-white rounded shadow">
                    <div class="p-3">
                        transit
                        Chart Here
                    </div>
                    <div class="px-3 py-2 border-top">
                        total?s
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bg-white rounded shadow">
                    <div class="p-3">
                        Live
                        Chart Here
                    </div>
                    <div class="px-3 py-2 border-top">
                        total?
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-white m-3 shadow-sm py-3" id="top-up">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">
                @include('wallet.nav')
            </div>
            <div class="col-auto">
                <button data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-main">Top Up</button>
            </div>
        </div>
        <div class="form-row mt-3">
            <div class="col-auto">
                <div class="form-group d-flex align-items-center">
                    <label for="">Show</label>
                    <select class="form-control mx-2" id="exampleFormControlSelect1">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                    <label>entries</label>
                </div>
            </div>
            <div class="col-auto">
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text bg-white border-main rounded-left">
                            <i class="far fa-calendar text-main"></i>
                        </span>
                    </div>
                    <input data-toggle="datepicker" type="text" name="name" class="form-control border-left-0 border-main" placeholder="Select a date" autocomplete="off">
                </div>
            </div>
            <div class="col-auto">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-white border-right-0 border-main text-main">Status:</span>
                    </div>
                    <select class="form-control border-left-0 border-main text-main" id="">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            <div class="col-aut0 ml-auto">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-white border-right-0" id=""><i class="fa fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control border-left-0" placeholder="Search here..." aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>

        <div class="table-responsive-md">

            <table class="table">
                <thead>
                    <tr>
                        <th class="border-top-0">From</th>
                        <th class="border-top-0">To</th>
                        <th class="border-top-0">Date</th>
                        <th class="border-top-0">Amount</th>
                        <th class="border-top-0">Actual Amount received</th>
                        <th class="border-top-0">Status</th>
                        <th class="border-top-0"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>12313123</td>
                        <td>12321312</td>
                        <td>2 Hours ago</td>
                        <td>USD 1.000</td>
                        <td>USD 1.038</td>
                        <td><span class="badge badge-warning w-100 py-3 text-white">Pending</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>5 Hours ago</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-danger w-100 py-3 text-white">Failed</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-success w-100 py-3 text-white">Completed</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-warning w-100 py-3 text-white">Pending</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-warning w-100 py-3 text-white">Pending</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-danger w-100 py-3 text-white">Failed</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-danger w-100 py-3 text-white">Failed</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-success w-100 py-3 text-white">Completed</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-success w-100 py-3 text-white">Completed</span></td>
                        <td>See Details</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>Account Number</td>
                        <td>Created</td>
                        <td>Fee</td>
                        <td>Fee</td>
                        <td><span class="badge badge-success w-100 py-3 text-white">Completed</span></td>
                        <td>See Details</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content p-3">
            <div class="modal-header border-0">
                <div>
                    <h4 class="modal-title font-weight-bolder">Withdrawal</h4>
                    <p class="font-weight-bold">Current Balance: <span class="h3 font-weight-bolder">USD 350</span></p>
                </div>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for=""></label>
                                <input type="text" class="form-control" placeholder="First name">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for=""></label>
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for=""></label>
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for=""></label>
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for=""></label>
                                <p>US 0</p>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for=""></label>
                                <p>USD 0</p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer border-0 mt-3">
                <button type="button" class="btn text-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-main">Proceed</button>
            </div>
        </div>
    </div>
</div>


<script>
    $('[data-toggle="datepicker"]').datepicker();
</script>
@endsection