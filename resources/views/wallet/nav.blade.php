<nav class="nav nav-page align-items-center">
  <a class="nav-link  @if(Request::segment(2) == 'top-up' || Request::segment(2) == '') {{ 'active'}} @endif" href="/wallet/top-up">Top Up</a>
  <a class="nav-link  @if(Request::segment(2) == 'withdrawal') {{ 'active'}} @endif" href="/wallet/withdrawal">Withdrawal</a>
  <a class="nav-link  @if(Request::segment(2) == 'funding-history') {{ 'active'}} @endif" href="/wallet/funding-history">Funding History</a>
  <a class="nav-link  @if(Request::segment(2) == 'zipmex') {{ 'active'}} @endif" href="/wallet/zipmex">Zipmex</a>
</nav>
