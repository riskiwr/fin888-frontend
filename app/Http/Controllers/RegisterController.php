<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FetchapiController as FETCH;

class RegisterController extends Controller
{
	public function register1(Request $request)
	{   

        $fields = array(
            'email' => $request->email,
			'username'=> $request->username,
			'upline_username'=>$request->upline_username,
			'password'=>$request->password,
			'retype_password'=>$request->retype_password,
        );

        $api_link = env('API').'validate-username/?username='.$request->username;
        $api_url = env('API').'register';

        $checkuser = FETCH::API($api_link, [], 'GET', '');

        if ($checkuser->status == 'error'){
        	// error
        	return json_encode($checkuser);

        }else {
        	$user = FETCH::API($api_url, $fields, 'POST', '');
	        if ($user->status == 'success') {
				return redirect('/register-2');
			} elseif ($user->status == 'error') {
				return redirect('/register-1');
			} else {
				return redirect('/register-1');
			}
        }
    }
}