<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
	public function login(Request $request)
	{   

        $fields = array(
            'username' => $request->user,
            'password' => $request->password,
        );

        $api_url = env('API').'login';
        $headers = array('Content-Type: application/x-www-form-urlencoded');
        $ch = curl_init($api_url);
        $postvars = http_build_query($fields);

        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $rs = json_decode($response,true);
        if($rs['data']['is_use_2fa']){
            return redirect('two-auth');
        }else{
            session([
                'token' => $rs['data']['token'],
                'login' => true,
            ]);
            return redirect('dashboard');
        }
        
    }
    public function FunctionName(Type $var = null)
    {
        # code...
    }
}