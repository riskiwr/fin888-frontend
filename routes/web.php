<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('lang/{language}', 'LocalizationController@switch')->name('localization.switch');
// AUTH
Route::view('/login', 'auths/login');
Route::view('/google-auth', 'auths/google-auth');
Route::view('/forgot-password', 'auths/forgot-password');
Route::view('/register-1', 'auths/register-step-1');
Route::view('/register-2', 'auths/register-step-2');
Route::view('/register-3', 'auths/register-step-3');

// Landing
Route::view('dashboard', 'dashboard/homepage');
Route::view('dashboard/homepage', 'dashboard/homepage');
Route::view('account-details', 'account-details/profile');
Route::view('account-details/profile', 'account-details/profile');
Route::view('account-details/nomination', 'account-details/nomination');
Route::view('account-details/documents', 'account-details/documents');
Route::view('account-details/change-password', 'account-details/change-password');
Route::view('wallet', 'wallet/top-up');
Route::view('wallet/top-up', 'wallet/top-up');
Route::view('wallet/withdrawal', 'wallet/withdrawal');
Route::view('trading-account', 'trading-account/trading-account');
Route::view('trading-account/trading-account', 'trading-account/trading-account');
Route::view('trading-account/trading-history', 'trading-account/trading-history');
Route::view('trading-account/trading-performance-report', 'trading-account/trading-performance-report');
Route::view('member-area', 'member-area/commision-payout');
Route::view('member-area/commision-payout', 'member-area/commision-payout');
Route::view('member-area/affiliate-network-payouts', 'member-area/affiliate-network-payouts');
Route::view('member-area/trading-rebate-payouts', 'member-area/trading-rebate-payouts');

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login', 'LoginController@login');

Route::resource('/dashboard','DashboardController');

Route::get('/logout', function () {
    session()->forget(['token','login']);
    return redirect('login');
});

Route::post('/register-1', 'RegisterController@register1');





